const cacheFunction = require("./cacheFunction");
//const underscore = require("underscore");
let power = (n) => Math.pow(n, 2);

let result = cacheFunction(power);
result(1,2,3,4);
result(234,43,4563,234)
result(12,1,5,4,6);


let sum = (n) => {
    return n+10;
}

let result1 = cacheFunction(sum);
result1(12,2,2,5,2);

