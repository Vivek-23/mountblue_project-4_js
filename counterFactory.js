// Return an object that has two methods called `increment` and `decrement`.
// `increment` should increment a counter variable in closure scope and return it.
// `decrement` should decrement the counter variable and return it.

function counterFactory(){
    let value = 0;
    return {
        increment: function(){
            return ++value;
        },
        decrement: function(){
            return --value;
        }
    }
}

module.exports = counterFactory;