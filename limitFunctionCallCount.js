// Should return a function that invokes `cb`.
// The returned function should only allow `cb` to be invoked `n` times.
// Returning null is acceptable if cb can't be returned

function limitFunctionCallCount(cb, n) {
    let count = n;
    return {
        invoke: function(){
            if(count-- > 0){
                cb(count);
            }else{
                null;
            }
        }
    }
}

module.exports = limitFunctionCallCount;