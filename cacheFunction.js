    // Should return a funciton that invokes `cb`.
    // A cache (object) should be kept in closure scope.
    // The cache should keep track of all arguments have been used to invoke this function.
    // If the returned function is invoked with arguments that it has already seen
    // then it should return the cached result and not invoke `cb` again.
    // `cb` should only ever be invoked once for a given set of arguments.

function cacheFunction(cb) {
    let cache = {};
    return (...params) => {
        for(let val = 0; val < params.length; val++){
            if(params[val] in cache){
                console.log('Fetching from cache: ', cache[params[val]]);
            }else{
                cache[params[val]] = cb(params[val]);
                console.log('Calculating result: ', cb(params[val]))
            }     
        }
    } 
}

module.exports = cacheFunction;