const counterFactory = require('./counterFactory');

let counter = counterFactory();
console.log(counter.increment());
console.log(counter.increment());
console.log(counter.decrement());
