const limitFunctionCallCount = require('./limitFunctionCallCount');

let counter = limitFunctionCallCount(() => {
    console.log('Callback invoked')
}, 2);
counter.invoke();
counter.invoke();
counter.invoke();
counter.invoke();